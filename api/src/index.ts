import express from "express";
import http from "http";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser"
import compression from "compression";
import cors from "cors";
import mongoose from "mongoose";
import * as dotenv from "dotenv";

import router from "./router";

dotenv.config();

const app = express();
app.use(
  cors({
    credentials: true,
    origin:"*"
  })
);
app.use(cookieParser());
app.use(bodyParser.json());
app.use(compression());

const server = http.createServer(app);
server.listen(process.env.PORT, () => {
  console.log("====================================");
  console.log(`server listening on port http://localhost:${process.env.PORT}`);
  console.log("====================================");
});
mongoose.Promise = Promise;
mongoose
  .connect(process.env.MONGODB_URL as string,{
    dbName:"ebooks"
  })
  .then((result) => {
    console.log("db connected!");
  })
  .catch((err) => console.log(err));

app.use("/", router());
